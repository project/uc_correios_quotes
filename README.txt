/**
 * uc_correios_quotes: Correios Quotes module for Drupal & Ubercart
 * Compatible with Drupal 7.x and Ubercart 3.x
 *
 * By Wanderson S. Reis aka wasare [http://www.ospath.com]
 * Version 2012/07/10
 * Licensed under the GPL version 2
 */

-- SUMMARY --

Correios Quotes é um módulo que habilita o cálculo de fretes pelos Correios para
o sistema de loja Ubercart (http://www.ubercart.org/what_is_ubercart).

Novas versões em http://drupal.org/project/uc_correios_quotes.


-- REQUIREMENTS --

Ubercart 3.x.
PHP 5.x.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

O CEP (código postal) padrão de origem da loja deve estar configurado corretamente
nas "Opções de cotação de frete" -> "Endereço padrão para buscar produtos",
utilize somente números para funcionar corretamente.

O CEP do endereço da loja (em "Configuração da loja" -> "Opções de contato") pode
ser diferente do CEP utilizado para o cálculo do frete.

É possível escolher entre dois tipos de mecanismos de cálculo de frete: o oficial dos
Correios e o calculador de frete grátis fornecido pelo site w21studio. Para uso da
segunda alternativa é necessário cadastrar-se site http://frete.w21studio.com/ para
obter o código de acesso.

O novo webservice oficial dos Correios exige o fornecimento das dimensões do pacote,
inclusive o formato, quando o frete é do tipo PAC. Este módulo permite a configuração
somente para pacotes com comprimento, altura e largura, e não valida as regras de
proporção definidas pelos Correios. Caso receba alguma mensagem de erro tente
configurações diferentes para evitar dimensões desproporcionais ou inválidas. As
dimensões configuradas influenciam na estimativa final do custo do frete.

-- CUSTOMIZATION AND TROUBLESHOOTING --

O cálculo é realizado de acordo com as informações do pedido do cliente. Para
maior compatiblidade o módulo permite duas formas de conexão ao webservice:
  - via função drupal_http_request que usa a função fsockopen do PHP que em
muitos casos é desabilitada pelo provedor por motivos de segurança e performace
  - via biblioteca cURL, que deve estar com o suporte ativado no PHP. Este tipo
conexão deve ser a melhor opção, pois apresentará melhor desempenho em lojas com
maior tráfego.

Se o frete não estiver sendo calculado e/ou retornar erro:

  - verifique se as configurações do CEP estão corretas em "Opções de cotação de
frete" -> "Endereço padrão para buscar produtos";
  - certifique-se que pelo menos um tipo de serviço (PAC ou SEDEX) esteja selecionado
em "Opções de cotação de frete" -> "Métodos de cotação" -> "Correios Quotes";
  - altere o método de conexão para usar a biblioteca cURL.


-- CONTACT --

Current maintainer:
* Wanderson S. Reis aka wasare - http://drupal.org/user/18262

-- CREDITS --

This project has been sponsored by ospath.com.

