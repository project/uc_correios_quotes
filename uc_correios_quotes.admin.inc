<?php

/**
 * @file
 * Correios Quotes administration menu items.
 *
 * Coded by Wanderson S. Reis aka wasare [http://www.ospath.com]
 *
 */


/**
 * Configures Correios Quotes services.
 */
function uc_correios_quotes_admin_settings($form, &$form_state) {

  $form = array();

  $form['uc_correios_quotes_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Correios Quotes Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#group'         => 'correios-quotes-settings',
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_default_engine'] = array(
    '#type' => 'select',
    '#title' => t('Default Service Engine'),
    '#description' => t('Fetch quotes from Correios (Brazilian Postal Service) or other service avaliable.'),
    '#default_value' => variable_get('uc_correios_quotes_default_engine', 0),
    '#options' => _uc_correios_engines_list(),
    '#required' => TRUE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Correios Services'),
    '#default_value' => variable_get('uc_correios_quotes_services', _uc_correios_services_list()),
    '#options' => _uc_correios_services_list(),
    '#description' => t('Select the kinds of shipping services will be avaliable for costumers.'),
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_display_time_estimate'] = array(
    '#default_value' => variable_get('uc_correios_quotes_display_time_estimate', FALSE),
    '#description' => t('The shipping time estimate will be displayed when avaliable.'),
    '#title' => t('Show shipping time estimate'),
    '#type' => 'checkbox',
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_send_declared_value'] = array(
    '#default_value' => variable_get('uc_correios_quotes_send_declared_value', FALSE),
    '#description' => t('The total order price will be sent as "declared value" when supported by webservice, this option increases the shipping cost.'),
    '#title' => t('Send order total price as "declared value"'),
    '#type' => 'checkbox',
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_curl_enabled'] = array(
    '#default_value' => variable_get('uc_correios_quotes_curl_enabled', FALSE),
    '#description' => t('Connect using cURL lib is more recommended, instead of using a drupal_http_request.'),
    '#title' => t('Use cURL lib to connection.'),
    '#type' => 'checkbox',
  );

  $form['uc_correios_quotes_options']['estimanting_quotes_by_product'] = array(
    '#type' => 'fieldset',
    '#title' => t('Estimate Shipping Cost On Product Page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['uc_correios_quotes_options']['estimanting_quotes_by_product']['uc_correios_quotes_estimanting_by_product_enabled'] = array(
    '#default_value' => variable_get('uc_correios_quotes_estimanting_by_product_enabled', FALSE),
    '#description' => t('Show a block to estimate shipping cost on each product page.'),
    '#title' => t('Enable estimate shipping cost on product page.'),
    '#type' => 'checkbox',
  );

  $form['uc_correios_quotes_options']['estimanting_quotes_by_product']['uc_correios_quotes_estimating_by_product_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message text'),
    '#description' => t('A custom message displayed when a estimative shipping cost was received on product page.'),
    '#default_value' => variable_get('uc_correios_quotes_estimating_by_product_message', t('The shipping cost estimate bellow refers only to this item by buying more items the shipping cost may be proportionately cheaper or even free. <br /> <strong> See the terms of the store and the minimum value for orders. </ strong>')),
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_w21studio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Frete w21studio setup'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_w21studio']['uc_correios_quotes_w21studio_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#size' => 10,
    '#maxlength' => 12,
    '#description' => t("Your account access token at w21studio free quotes service. <strong>Register to get a access token at</strong> <a href=\"@w21studio_link\" target=\"_blank\">@w21studio_link</a>.", array('@w21studio_link' => 'http://frete.w21studio.com/')),
    '#default_value' => variable_get('uc_correios_quotes_w21studio_token', ''),
    '#required' => FALSE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_pac'] = array(
    '#type' => 'fieldset',
    '#title' => t('Correios PAC setup'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_pac']['uc_correios_quotes_pac_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Package length'),
    '#size' => 6,
    '#maxlength' => 8,
    '#description' => t("Package length range 16 to 60 centimeters."),
    '#default_value' => variable_get('uc_correios_quotes_pac_length', 16),
    '#required' => TRUE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_pac']['uc_correios_quotes_pac_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Package height'),
    '#size' => 6,
    '#maxlength' => 8,
    '#description' => t("Package height range 2 to 60 centimeters."),
    '#default_value' => variable_get('uc_correios_quotes_pac_height', 2),
    '#required' => TRUE,
  );

  $form['uc_correios_quotes_options']['uc_correios_quotes_pac']['uc_correios_quotes_pac_depth'] = array(
    '#type' => 'textfield',
    '#title' => t('Package depth'),
    '#size' => 6,
    '#maxlength' => 8,
    '#description' => t("Package depth range 5 to 60 centimeters."),
    '#default_value' => variable_get('uc_correios_quotes_pac_depth', 11),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
